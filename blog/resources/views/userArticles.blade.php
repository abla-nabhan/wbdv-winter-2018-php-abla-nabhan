@extends('layouts.app')
@extends('layout')
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

<h1>{{$user->name}}'s Articles</h1>

<div class="comments-section">
  <?php foreach($articles as $article): ?>

  <div class="comment-container">
    <div class="comment-picture">
        <img class="user-picture" src='<?php echo $article->image ?>'></img>
    </div>
    <div class="comment-content">

    <hr>
    <?php echo $article->content ?>
    <br><br>
    <span class="article-author"> <a href="/articles/{{$article->user->id}}"><?php echo $article->user->name?></a></span>
    <span class="like-count"><?php echo count($article->likes) ?></span>
  <?php if ($article->isLikedByCurrentUser()): ?>
      <a href="/articles/{{$article->id}}/like/toggle">unlike</a>
      <?php else: ?>
        <a href="/articles/{{ $article->id }}/like/toggle">like</a>
  <?php endif; ?>

    </div>
  <br>

  </div>
  <?php foreach ($article->comment as $comment): ?>
  <span class="name">Comments: </span>
  <?php echo $comment->content ?> <br>
  <span class="name">Comment By: </span>
  <?php echo $comment->user->name ?> <br>
  <span class="name">Date: </span>
  <?php echo $comment->updated_at->diffForHumans() ?>
  <?php endforeach ?>
  <hr>

  <form method="post" action="/comment">
  <?php echo csrf_field() ?>
  <input type="text" name="comment" placeholder="Comment here...">
  <input type="hidden" name="articleId" value="{{$article->id}}">

  <input type="submit" name="" value="Post Comment">

  </form>
  <?php endforeach ?>

</div>

</body>
</html>
