<div class="form-group">
  <label for="<?php echo $name ?>"><?php echo $label ?></label>
  <textarea type="text"
            name="content"
            rows="4" cols="40"
            placeholder="Type here..."
            class="form-control
    <?php echo $errors->has($name) ? 'is-invalid' : '' ?>"><?php echo old('content')?></textarea>

  <?php if($errors->has($name)): ?>
     <span class="invalid-feedback"><?php echo $errors->first($name) ?>
  <?php endif; ?>

</div>
