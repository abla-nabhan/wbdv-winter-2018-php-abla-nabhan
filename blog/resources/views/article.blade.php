@extends('layout')
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/app.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <title></title>

  </head>
  <body>

    <?php if($message = session('message')): ?>
        <div class="alert alert-success">
          <?php echo $message ?>
        </div>
    <?php endif; ?>


    <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $error): ?>
                        <li><?php echo $error ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
    <?php endif; ?>


      <h1>Create Article</h1>

      <form method='post'>
        <?php echo csrf_field() ?>

    @include('forms.text', [
        'label' => 'Title',
        'name' => 'title'
      ])

    @include('forms.textarea', [
        'label' => 'Content',
        'name' => 'content'
      ])

        <br><br>  <input type="submit" name="" value="submit">
      </form>





  </body>
</html>
