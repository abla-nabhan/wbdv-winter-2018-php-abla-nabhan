
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/app.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <title></title>
  </head>
  <body>
    <div class="container">

<!-- Navigation -->
      <div class="nav-bar">
        <ul>
          <li>Explore</li>
          <li>Plan <i class="fas fa-caret-down"></i></li>
          <li> <a href="/article">Create Article</a> <i class="fas fa-caret-down"></i></li>
          <a href="/"><li class="logo"> </li> </a>
          <li> <a href="/contact">Contact Us</a></li>
          <li class="sign-up"> <a href="/register">Sign up</a><i class="fas fa-caret-down"></i></li>
          <li><a href="/login">Log in</a> </li>
        </ul>
      </div>
<!-- Information bar & search -->
      <div class="secondary-bar">
        <ul>
          <li>Canada</li>
          <li>Alberta</li>
          <li class="long-text">Canmore Nordic Centre Provincial Park</li>
          <li class="long-text">Ha Ling Trail to Ha Ling Peak</li>
          <li class="spacer"></li>
          <li class="search-field">Enter a city, park or trail name...</li>
          <li class="search-button"><button type="button" class="search-button">Search</button></li>
        </ul>
      </div>
<!-- Hero Image -->
      <div class="hero-image">
        <div class="hero-content">
          <h1>Ha Ling Trail to Ha Ling Peak</h1>
          <button type="button" class="rating-button">HARD</button>
          <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
          396 reviews <br/>
          #1 of 6 trails in Canmore Nordic Centre Provincial Park
          <hr>
          <ul>
            <li><i class="far fa-heart"></i> Favourite</li>
            <li><i class="fas fa-long-arrow-alt-up"></i> Directions</li>
            <li><i class="fas fa-cloud-download-alt"></i> PDF Map</li>
            <li><i class="fas fa-print"></i> Print Map</li>
            <li><i class="fas fa-mobile-alt"></i> Send to Phone</li>
            <li><i class="fas fa-ellipsis-h"></i> More <i class="fas fa-caret-down"></i></li>
          </ul>
        </div>
      </div>

<!-- Left Column Items -->
<div class="body-content">

<!--  Trail Description -->
<div class="left-column container">

          <p>Ha Ling Trail to Ha Ling Peak is a 5.8 kilometer heavily trafficked
          out and back trail located near Canmore, Alberta, Canada that features
          a great forest setting and is rated as difficult. The trail offers
          a number of activity options and is best used from May until October.
          Dogs are also able to use this trail.
          </p>
          <hr>
          <div class="trail-stats">
            <ul>
              <li><i class="fas fa-map-marker-alt"></i> DISTANCE <br> 5.8 km </li>
              <li><i class="fas fa-chart-line"></i> ELEVATION GAIN <br> 729 m </li>
              <li><i class="fas fa-arrows-alt-v"></i> ROUTE TYPE <br> Out & Back </li>
            </ul>
          </div>
          <div class="trail-tags">
            <ul class="trail-tag-list">
              <li>dog friendly</li>
              <li>hiking</li>
              <li>mountain biking</li>
              <li>snowshoeing</li>
              <li>trail running</li>
              <li>forest</li>
              <li>views</li>
              <li>scramble</li>
              <li>snow</li>
            </ul>
          </div>

<!-- Comment Section -->
          <div class="comment-headings">
            <ul>
              <li>Reviews (396)</li>
              <li>Photos (255)</li>
              <li>Recordings (198)</li>
            </ul>
          </div>

          <div class="comment-filters">
            <ul>
              <li>AllTrails Sort |</li>
              <li>Newest First |</li>
              <li>Oldest First |</li>
              <li>Highest Rated |</li>
              <li>Lowest Rated</li>
            </ul>
          </div>

        <!-- <div class="comments-container container"> -->
<!-- <div class="comments-container"> -->

        <div class="comments-section">
          <?php foreach($articles as $article): ?>

          <div class="comment-container">
            <div class="comment-picture">
                <img class="user-picture" src='<?php echo $article->image ?>'></img>
            </div>
            <div class="comment-content">
              <ul class="name-stars">
                <li><?php echo $article->title ?></li>
                <li class="spacer"></li>
                <li class="spacer"></li>
                <li class="spacer"></li>
                <li class="spacer"></li>
                <li>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                </li>
              </ul>
            <ul>
              <li class="comment-tag">hiking</li>
              <li class="spacer"></li>
              <li class="spacer"></li>
              <li class="spacer"></li>
              <li>
                <?php echo $article->date ?>
              </li>
            </ul>
            <hr>
            <?php echo $article->content ?>
            <br><br>
            <span class="article-author"> <a href="/articles/{{$article->user->id}}"><?php echo $article->user->name?></span></a>
            <span class="like-count"><?php echo count($article->likes) ?></span>
          <?php if ($article->isLikedByCurrentUser()): ?>
              <a href="/articles/{{$article->id}}/like/toggle">unlike</a>
              <?php else: ?>
                <a href="/articles/{{ $article->id }}/like/toggle">like</a>
          <?php endif; ?>

            </div>
          <br>

          </div>
          <?php foreach ($article->comment as $comment): ?>
          <span class="name">Comments: </span>
          <?php echo $comment->content ?> <br>
          <span class="name">User: </span>
          <?php echo $comment->user->name ?> <br>
          <span class="name">Date: </span>
          <?php echo $comment->updated_at->diffForHumans() ?>
          <?php endforeach ?>
          <hr>

          <form method="post" action="/comment">
          <?php echo csrf_field() ?>
          <input type="text" name="comment" placeholder="Comment here...">
          <input type="hidden" name="articleId" value="{{$article->id}}">

          <input type="submit" name="" value="Post Comment">

          </form>
          <?php endforeach ?>

      </div>
        <br>
      <!-- </div> -->


      <!-- </div> -->

</div>

<!-- Right Column Items -->
<div class="right-column container">
      <div class="static-map">
        <button type="button" class="map-button">VIEW FULL MAP</button>
      </div>
      <hr>
      <h3>Completed <button type="button" class="completed-button">991</button></h3>
      <div class="completed-pictures">
      </div>
      <hr>
      <h3>Weather</h3>
      <div class="weather">
          <ul>

          </ul>
          <ul class="weather-icons">
            <li><i class="fas fa-cloud fa-3x"></i></li>
            <li><i class="fas fa-snowflake fa-3x"></i></li>
            <li><i class="fas fa-snowflake fa-3x"></i></li>
            <li><i class="fas fa-sun fa-3x"></i></li>
          </ul>
          <ul>

      </div>
      <hr>
      <h3>Nearby Trails</h3>

      <div class="trails-pictures">
        <div class="trail-pictures-content">

          <button type="button" class="rating-button">HARD</button>
          <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
          (396)<br/>
          #1 of 6 trails in Canmore Nordic Centre Provincial Park
      </div>
      </div>
</div>




    </div>
  </body>
</html>
