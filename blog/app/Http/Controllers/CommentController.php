<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Article;
use App\Models\Comment;

class CommentController extends Controller
{
  public function create(){
    if(!\Auth::check()){
      return redirect('/login');
    }
    return view('comment');
  }
  public function store() {
    if(!\Auth::check()) {
      return redirect('/login');
    }

    $request = request();
    $loggedInUser = $request->user();
    $data = request()->all();

    $result = $request->validate([
      'comment' => 'required|max:255'
    ], [
      'comment.required' => 'Please add a comment.'
    ]);

    $comment = new Comment();
    $comment->user_id = $loggedInUser->id;
    $comment->article_id = $data['articleId'];
    $comment->content = $data['comment'];
    $comment->save();


    return redirect('/')->with('message', 'Your comment was posted!');
  }


}
