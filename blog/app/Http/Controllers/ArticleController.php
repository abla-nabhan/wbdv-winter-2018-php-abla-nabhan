<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Comment;
use App\User;

class ArticleController extends Controller
{
    public function create() {

      if (!\Auth::check()){
        return redirect('/login');
      }

      return view('article');
    }

    public function store()
    {


      $request = request();

      $result = $request->validate([
     'title' => 'required|max:255',
     'content' => 'required|max:1000'
   ], [
     'title.required' => 'Please enter a title.',
     'content.required' => 'Please include content in your article'
   ]);

   $data = $request->all();

   $loggedInUser = $request->user();

   $article = new Article();
   $article->user_id = $loggedInUser->id;
   $article->title = $data['title'];
   $article->content = $data['content'];

   $article->save();

   return redirect('/article')
   ->with('message', 'Your article was created congrats');
    }

    public function userArticles($userId)
    {
      $user = User::where('name', $userId)
      ->orWhere('id', $userId)
      ->first();

      if (!$user) {
        abort(404);
      }

      $articles = $user->article;
//$articles = Article::all();

      return view('userArticles', [
        'user' => $user,
        'articles' => $articles
      ]);
    }

    public function toggleLike($articleId)
    {
      $user = request()->user();
      $article = Article::find($articleId);

      if ($article->isLikedByCurrentUser()) {
        $article->likes()->detach($user);
      } else {
        $article->likes()->attach($user);
      }

      return back()
            ->with('message', 'You Liked An Article');
    }

}
